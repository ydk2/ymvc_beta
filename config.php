<?php
namespace {

define('__APP__',dirname(__FILE__));
define('VEXT','.phtml');
define('XVEXT','.xsl');
$template="new";
$model='/application/models/model';
define('SQL','posql');
define('DEBUG',null);
define('MEDIA_LEN',100);
define('INDEX', 'start');
setlocale(LC_ALL,'pl_PL.UTF-8');
define('LANG', 'en');
define('DBPREFIX', '');
require_once __APP__.DIRECTORY_SEPARATOR.'system'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'constants.php';

}
?>