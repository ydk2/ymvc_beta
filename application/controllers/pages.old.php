<?php
namespace Application\controllers;
use \System\core\Helper as Helper;
use \System\controller\template as template;

class Pages extends \System\core\controllers {
	//    public $model;
	//	public $view;
	public $header;
	public $footer;
	public $heading;

	public function __construct($model) {
		/*
		 $this->name_model = $model;
		 $this->model = new $model();
		 $this->view = $view;
		 *
		 */
		//$this->error = 10970;
		
		
		parent::__construct($model);
		//echo $this->model->error; // = $this->error;
		//$this->ViewSync();
		//parent::Sync();
		
		$this->access = 1000;
		//echo $this->view;
		if(isset($_GET['page'])){
			if($_GET['page']== 'view') {
				$this->error = 7420;

				//$this->view=VIEWS.$_GET['page'];
			} 
			
		} 
		
		if(!$this->CheckView($this->view)){
			 $this->view = VIEWS."pages";
			 $this->error=0;
		}
		if(Helper::session('access') > $this->access) {
			
			$this->ViewData('header','Ups! Something wrong!!!');
			$this->ViewData('content','<b>Catch error code:</b> '.$this->error);
			$this->ViewData('subheader',$this->model->lang_strings['Subtitle of this page']);
			//$this->view = VIEWS.'errors/errors';
			$this->error=300;
			//$this->Exceptions($this->model,SVIEWS.'errors/e403',CONTROLLERS.'errors/errors');
			//return;
		} 
		
		if($this->error > 0){
			$this->view = VIEWS.'empty';
			//return;
			//$this->ViewData('header','Ups! Something wrong!!!');
			$this->model->error=$this->error;
			if($this->error == 300) $this->Exceptions($this->model,SVIEWS.'empty',SCONTROLLERS.'errors/errors');
			if($this->error == 5678) $this->Exceptions($this->model,SVIEWS.'errors/e403',SCONTROLLERS.'errors/errors');
			//if($this->error == 404) $this->Exceptions($this->model,SVIEWS.'errors/e404',CONTROLLERS.'errors/e404');
			if($this->error == 7420) $this->Exceptions($this->model,SVIEWS.'empty',SCONTROLLERS.'errors/errors');


		} 
		if($this->error == 0){
 //$xsl = new \DOMDocument();
   //$xsl->load("pages.xsl");

   $xsl = new \SimpleXMLElement('pages.xsl', NULL, TRUE);
   $view = new \XSLTProcessor();
   if (!$view->hasExsltSupport()) {
    $this->Exceptions($this->model,SVIEWS.'empty',SCONTROLLERS.'errors/errors'); //'EXSLT support not available');
	}
   $view->importStylesheet($xsl);
   $view->registerPHPFunctions();
   //echo $proc->transformToXML($xmlDoc);			
			//$this->ViewData('content',"Content B ".$this->me." & ".$this->view);
			if($_GET['pages']=="") $_GET['pages']='start';
			//echo $_GET['pages'];
			$view->setParameter('', 'lang', 'pl');
			$link = $this->model->get_page($_GET['pages']);
			$this->ViewData('header',$this->model->page_title);
			$this->ViewData('subheader',$this->model->lang_strings['Subtitle of this page']);
			$this->ViewData('footer',$this->model->lang_strings['Footer Header']);
			//
			//$xml = $this->generateXMLElement($link, null, 'data');
/*
			$doc = new \DOMDocument( '1.0' );
    		$doc->formatOutput = true;
    		$doc->preserveWhiteSpace = true;
			
    		$root = $doc->createElement( 'data' );
			
			$title = $doc->createElement( 'title' ,'Bla bla bla');
        	$root->appendChild( $title);
			
			$content = $doc->createElement( 'content' ,'Bla bla bla');
        	$root->appendChild( $content );
			
			$id = $doc->createElement( 'id' ,'1');
        	$root->appendChild( $id );

			$lang = $doc->createElement( 'lang' ,'en');
        	$root->appendChild( $lang );
			
			foreach ($link as $key => $value) {
				# code...
				$node = $doc->createElement( $key,$value);
        		$root->appendChild( $node );
			}
    		$doc->appendChild( $root );
*/
			$q =FALSE;
			if(file_exists("pages.xml") && $q ){
			$data = new \SimpleXMLElement('pages.xml', NULL, TRUE) ;
			$proc->setParameter('', 'langs', 'ruski');
			//unlink("pages.xml");
			} else {
			$data = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data></data>', null, false);
			//$data->title = 'Hurra title';
			foreach ($link as $key => $value) {
				# code...
				$data->$key=$value;
			}
			//var_dump($data);
			$data->asXML("pages.xml");
			}
			//echo $proc->getParameter('', 'langs');
			//echo htmlspecialchars( $doc->saveXML());
			//$xmlDoc->loadxml($xml->asXML());
			$this->ViewData('content',$link['content']."<p>&nbsp;</p>".$view->transformToXML($data));

			//var_dump($link);
			$this->Viewdata('footer','New Footer');
			unset($xsl);
			unset($view);
			unset($data);
		}
			
		//echo var_dump($this);
	}
	static public function methodName($someArgument)
      {
         return "resultis:".$someArgument;
      }
	 static  public function method($someArgument)
      {
         return "result is: ".$someArgument;
      }
function generateXMLElement($elements, $rootNode = null, $rootNodeName = 'xml')
{
    if (!$rootNode)
    {
        $rootNode = new \SimpleXMLElement('<'.$rootNodeName.'/>');
    }
    foreach ($elements as $key => $val)
    {
        if (is_array($val))
        {
            $childElem = $rootNode->addChild($key);
            generateXMLElement($val, $childElem);
        }
        else
        {
            $childElem = $rootNode->addChild($key, $val);
        }
    }
    return $rootNode;
}

}
?>
<?php

  

?>
<?php
/**
* @param  $xml
* @param  $xsl
* @return string xml
*/
function transform($xml, $xsl) {
   $xslt = new XSLTProcessor();
   $xslt->importStylesheet(new  SimpleXMLElement($xsl));
   return $xslt->transformToXml(new SimpleXMLElement($xml));
}
?>