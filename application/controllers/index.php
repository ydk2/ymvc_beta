<?php
namespace Application\controllers;
use \System\core\Helper as Helper;

class index extends \System\core\controllers {
	//    public $model;
	//	public $view;
	public $header;
	public $footer;
	public $heading;

	public function __construct($model) {
		/*
		 $this->name_model = $model;
		 $this->model = new $model();
		 $this->view = $view;
		 *
		 */
		parent::__construct($model);
		$this->access = 1000;

		Helper::session_set('locale','pl');
		if(!$this->CheckView($this->view)) $this->error = 404;
		$this->ViewData('header',$this->model->lang_strings['Ymvc <small>System</small>']);
		$this->ViewData('subheader',$this->model->lang_strings['Subtitle of this page']);
		$this->ViewData('footer',$this->model->lang_strings['Footer Header']);
		//$core = new \System\Core\Views(MODELS.'model',VIEWS.'pages',CONTROLLERS.'content');
		//$this->ViewData('content',$core->view());

		//$this->view = SVIEWS.'errors/405';
		

		//echo var_dump($this);
		if($this->error > 0){
			//$this->view = VIEWS.'errors/e4x04';
			//$this->ViewData('header','Ups! Something wrong!!!');
			$this->model->error=$this->error;
			if($this->error == 403) $this->Exceptions($this->model,SVIEWS.'errors/e403',SCONTROLLERS.'errors/errors');
			if($this->error == 404) $this->Exceptions($this->model,SVIEWS.'errors/e404',CONTROLLERS.'errors/e404');
			if($this->error == 7420) $this->Exceptions($this->model,SVIEWS.'errors/errors',SCONTROLLERS.'errors/errors');
			//endif;
		}
	}


}
?>