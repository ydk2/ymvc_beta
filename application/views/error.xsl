<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" indent="yes"/>
	<xsl:param name="content"/>
	<xsl:param name="langs">Polski</xsl:param>
	<xsl:template match="/">
		<!--<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>-->
		<div>
			<xsl:for-each select="data">

					<div>
						<xsl:value-of select="message"  disable-output-escaping="yes"/>
					</div>
					<div>
						<xsl:value-of select="error"/>
					</div>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>