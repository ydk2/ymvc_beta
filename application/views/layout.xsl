<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" doctype-system="about:legacy-compat" omit-xml-declaration="yes" indent="yes" />
	<xsl:param name="lang" select="pls"/>
	<xsl:param name="content" />
	<xsl:param name="dump" />
	<xsl:param name="langs">Polski</xsl:param>
	<xsl:param name="view"/>
	<xsl:template match="/">
		<xsl:param name="action">Sec</xsl:param>
		<!--<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>-->
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<title>XSLView Layout</title>
				<script>
					<![CDATA[ 
				body.onload{
				alert("kupa");
				}
				]]>
				</script>
			</head>
			<body>
				<div>
					<div>
							<xsl:choose>
								<xsl:when test="$view = 'a'">
									<strong name="langs">Polski</strong>
								</xsl:when>
								<xsl:when test="$view = 'b'">
									<strong name="langs">bla bla bla</strong>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="/layout/item">
										<xsl:if test="$lang = 'pl'">
										<h2><xsl:value-of select="@title" /></h2>
											<div>
												<xsl:value-of select="c/node()" disable-output-escaping="yes"/>
											</div>
										</xsl:if>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
					</div>
					<div>
						<xsl:value-of select="$dump" disable-output-escaping="yes"/>
					</div>
				</div>
				<div>		Exec Time: 					
					<xsl:value-of select="data/time"/>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>