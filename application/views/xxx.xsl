<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" indent="yes"/>
	<xsl:param name="test" select="php:function ($self,'get_post','ccc')"  xmlns:php="http://php.net/xsl"/>
	<xsl:param name="dump"/>
	<xsl:param name="langs">Polski</xsl:param>
	<xsl:template match="/">
	<div>
		<h2>XXX</h2>
		<xsl:if test='$dump'>
		<pre>
		<xsl:value-of select="$dump"/>
		</pre>
		</xsl:if>
	</div>
		<!--<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>-->

		<div>
		<xsl:value-of select="$langs"   disable-output-escaping="yes"/><br/>
		<xsl:value-of select="data/content"   disable-output-escaping="yes"/><br/>
		<pre>
		<xsl:value-of select="php:function ($self,'get_post','ccc')"  xmlns:php="http://php.net/xsl" disable-output-escaping="yes"/>
		</pre>
		</div>
		<div>
		<xsl:value-of select="data/time"/>
		</div>
	</xsl:template>
</xsl:stylesheet>