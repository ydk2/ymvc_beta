<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" indent="yes"/>
	<xsl:param name="content"/>
	<xsl:param name="test"/>
	<xsl:param name="dump"/>
	<xsl:param name="call"/>
	<xsl:param name="langs">Polski</xsl:param>
	<xsl:template match="/">
	<div>
		<h2>PAGE</h2>
		<xsl:if test='$dump'>
		<pre>
		<xsl:value-of select="$dump"/>
		</pre>
		</xsl:if>
	</div>
		<!--<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;
</xsl:text>-->
		<div>
			<xsl:for-each select="data">

					<h3>
						<xsl:value-of select="title"/>
					</h3>
					<div>
						<xsl:value-of select="content"  disable-output-escaping="yes"/>
					</div>
			</xsl:for-each>
		</div>

		<div>
		<xsl:value-of select="php:function ($self,'_clone','aaa;bbb')"  xmlns:php="http://php.net/xsl"/>
		
		<pre>
		<xsl:value-of select="data/message"/>
		</pre>
		<xsl:value-of select="php:function ($self,'get_post','ccc;ddd')"  xmlns:php="http://php.net/xsl"/>
		
		<pre>
		<xsl:value-of select="data/message"/>
		</pre>
		<xsl:value-of select="php:function ($self,'_clone','eee;fff')"  xmlns:php="http://php.net/xsl"/>
		
		<pre>
		<xsl:value-of select="data/message"/>
		</pre>
		
		</div>
		<div>
		<xsl:value-of select="data/time"/>
		</div>
	</xsl:template>
</xsl:stylesheet>