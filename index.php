<?php
namespace {
	require_once "config.php";
	require_once "bootstrap.php";
	use \System\Core\Intl as Intl;
	use \System\Core\Helper as Helper;
	use \Libraries\Classes\toAscii as toAscii;
	use \System\Core\Router as Router;
	use \System\Data\Config\Config as Config;
//phpinfo();
	Config::Init();
	Helper::Session_Start();

function main(){
	$model = new \SimpleXMLElement(__APP__.STORE.'layout.xml', null, TRUE);
	$core = new \application\controllers\Page(VIEWS.'page');
	if($core->error == 404) $core->Exceptions($model,VIEWS.'error',CONTROLLERS.'error');
//	if($core->error == 404) $core->Exceptions($model,VIEWS.'error',CONTROLLERS.'error');
//	if($core->error == 501) $core->Exceptions($model,VIEWS.'error',CONTROLLERS.'error');
	$second = new \application\controllers\xxx($model,VIEWS.'xxx');
	if($second->error > 0) $second->Exceptions($model,VIEWS.'error',CONTROLLERS.'error');

	$core->Show();
	$second->Show();
}
function dump($value='') {
	ob_start();
	var_dump($value);
	return ob_get_clean();
}
main();
} // end namespace
?>