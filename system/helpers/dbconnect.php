<?php
namespace System\helpers;
/**
 *
 */

class dbconnect {
	public $base;
	public function __construct($engin, $database, $host = 'localhost', $user = NULL, $pass = NULL) {
		try {
			if ($engin == 'posql') {
				require_once __APP__.VENDORS.'posql.php';
				$database_name = __APP__ . '/data/' . $database . '.db';
				// try connect
				//$sql = SQL;
				
				$this->base = new \Posql($database_name);
				//$this -> base->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

				if ($this -> base -> isError()) {
					throw new \Exception('Can\'t connect to Database.');
					abort($this -> base);
				}

			}
			if ($engin != 'posql') {
				if ($user === NULL || $pass === NULL) {
					throw new \Exception('User and Password not filed.');
				}
				$this -> base = new \PDO($engin.':host=' . $host . ';dbname=' . $database, $user, $pass);
				$this -> base->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				//if ($this -> base -> isError()) {
				//	abort($this -> base);
				//}

			}
		} catch (Exception $e) {
			print "Error!: " . $e -> getMessage() . "<br/>";
			die();
		}

	}

	public function __destruct() {
		$this -> base = NULL;
		unset($this -> base);
	}

}
?>