<?php
namespace System\helpers;
/**
 * debug class static
 */
class Debug {
	
static public function dump($value) {
	if (defined('DEBUG') && null !== DEBUG) {
	ob_start();
	var_dump($value);
	$retval = ob_get_contents();
	ob_clean();
	return $retval;
	}
}
}

?>