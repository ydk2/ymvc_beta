<?php
namespace System\models;
class data {
	public $title;
	public $date;
	public $content;
	public $path;
	public $parent_id;
	public $id;
	
	public function __construct($id,$title,$parent, $date,$path, $content)  
    {
    	$this->id = $id;  
        $this->title = $title;
	    $this->parent_id = $parent;
	    $this->date = $date;
		$this->path = $path;
	    $this->content = $content;
    } 
}

?>