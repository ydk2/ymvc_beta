<?php
namespace {
// errors codes
define('ERR_SUCCESS',0);
define('ERR_CVACCESS',403);
define('ERR_CVENABLE',502);
define('ERR_CVDISABLE',501);
define('ERR_CVEXIST',404);


define('ACCESS_ANY',1000);
define('ACCESS_USER',500);
define('ACCESS_MODERATOR',200);
define('ACCESS_EDITOR',100);
define('ACCESS_SYSTEM',10);
define('ACCESS_ADMIN',0);

$url=(isset($_SERVER['HTTPS']))?'https://'.dirname($_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']):'http://'.dirname($_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);
define('HOST_URL',$url);

define('SCONTROLLERS','/system/controllers/');
define('SMODELS','/system/models/');
define('SVIEWS','/system/views/');
define('SSTORE','/system/data/stored/');
define('SDATA','/system/data/');

define('MCONTROLLERS','/modules/controllers/');
define('MMODELS','/modules/models/');
define('MVIEWS','/modules/views/');
define('MSTORE','/modules/data/stored/');

define('CONTROLLERS','/application/controllers/');
define('MODELS','/application/models/');
define('VIEWS','/application/views/');
define('STORE','/application/data/stored/');
define('ADATA','/application/data/');

define('LIBRARIES','/libraries/');
define('MODULES','/modules/');
define('DATA','/data/');

define('SYS_LIBRARIES','/system/libraries/classes/');
define('SYS_MODULES','/system/modules/classes/');
define('VENDORS','/system/modules/vendors/');
define('TEMPLATES', '/templates/');
define('USES', '/data/config/uses.php');


}
?>