<?php
namespace System\core;
/**
 *
 */

class Views extends \System\core\Controllers {

	public $action;

	function __construct($view) {
		$argsv = func_get_args();
		$argsc = func_num_args();
		if (method_exists($this, $f = 'start_' . $argsc)) {
			return call_user_func_array(array($this, $f), $argsv);
		}
	}

	private function start_1($view = '') {
		$this->view = $view;
	}

	private function start_2($view,$controller) {
		$this->view = $view;
		$model = new \StdClass();
		$this->register($controller,$model);
	}
	
	private function start_3($model,$view,$controller) {
		$this->view = $view;
		$this->register($controller,$model);
	}

	public function show($view = NULL) {	
		echo $this->view($view);
	}
	
	public function view($view = NULL) {
		try {
		ob_start();

		if(is_null($this->action)) throw new SystemException('Error',500);
		if(isset($this->action->exception)){	
		if($this->action->error > 0) {
			$this->action->view = VIEWS.'empty';
			throw new SystemException('Error',$this->action->error);
		}
		}
		if($this->view !== $this->action->view){
			$this->view=$this->action->view;
		}

		if ($view == NULL) {
			if ($this->CheckView($this -> view)) {
				require (__APP__ . $this -> view . VEXT);
			} else {
				$this->action->error = 404;
				throw new SystemException('Error',404);
			}
		} else {
			if ($this->CheckView($view)) {
				require (__APP__ . $view . VEXT);
			} else {
				$this->action->error = 404;
				throw new SystemException('Error',404);
			}
		}

		$out = ob_get_clean();
		return $out;
		} catch (SystemException $e){
			$this->error = $e->GetCode();
				if(isset($this->action->exception)){
					return $this->action->exception->view();
				}
				return FALSE;			
		}
	}

	public function ViewData() {
			$argsv = func_get_args();
			$argsc = func_num_args();
				return call_user_func_array(array($this->action, __FUNCTION__), $argsv);
		}

	public function __destruct() {
		foreach ($this as $key => $value) {
			$this->$key = NULL;
			unset($this->$key);
		}
		unset($this);
		clearstatcache();
	}
}
?>