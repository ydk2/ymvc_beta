<?php
namespace System\core ;

class Controllers {
	const ACCESS_ANY = 1000;
	const ACCESS_USER = 500;
	const ACCESS_EDITOR = 300;
	const ACCESS_MODERATOR = 100;
	const ACCESS_SYSTEM = 1;
	const ACCESS_ADMIN = 0;
	
	public $me;
	public $access;

	public $model;
	public $view;
	public $error;

public function __construct($model)  
    {
		try {
		$this->me=get_class($this);	
		//$this->view = parent::$view;

		if (is_object($model)) {
			$this->model = $model;
		} else {
			$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model );   
			if(!class_exists($model)) throw new SystemException('Error',504);
        	$this->model = new $model();
		}


		if (!isset($this -> access)):
		$this -> access = $this::ACCESS_ANY;
		endif;

		if(isset($this->exception)){
		if($this->error > 0) {
			$this->view = VIEWS.'empty';
			throw new SystemException('Error',$this->error);
		}
		}
		if (is_null($this -> error)):
		$this -> error = 0;
		endif;

		$this->ViewSync(); 
		} catch (SystemException $e){
			$this->error = $e->GetCode();
			if(isset($this->exception)){
				return FALSE; //$this->exception->view();
			}
			return FALSE;
		}
		//$this->invoke();	
    } 
	
	public function register($controller, $model) {
		try {
       	if (is_object($model)) {
			$this->model = $model;
		} else {
			$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model );  
			if(!class_exists($model)) throw new SystemException('Error',504);
        	$this->model = new $model();
		}

       	if (is_object($controller)) {
			$this->action = $controller;
		} else {
			$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
			if(!class_exists($controller)) throw new SystemException('Error',503);
			$this->preSync();
			$this->action = new $controller($this->model); 
			if(!$this->action) throw new SystemException('Error',500);
		}

		$this->access=min($this->action->access,$this->access);
		if(is_null($this->action->view)){
		$this->action->view=$this->view;
		}
		$this->me=$this->action->me;
		//if(isset($this->action->exception)){
		if($this->action->error > 0) throw new SystemException('Error',$this->action->error);
		//}
		//if($this->action->error > 0) throw new SystemException('Error',20300);
		} catch (SystemException $e){
			$this->error = $e->GetCode();
			if(isset($this->action->exception)){
				//return $this->action->exception->view();
			}
			return FALSE;
		}
	}
	public function ViewSync() {
		$this->view = $this->model->view;	
	}
	private function preSync() {
		$this->model->view = $this->view;
	}
	public function Exceptions($model,$view,$controller) {
		$this->exception = new \System\Core\Views($view);
		$this->exception->register($controller,$model);
	}

	public function ViewData() {
			$argsv = func_get_args();
			$argsc = func_num_args();
			if (method_exists($this, $f = 'Data_' . $argsc)) {
				return call_user_func_array(array($this, $f), $argsv);
			}
		}

		private function Data_1($value = '') {
			return (isset($this -> viewdata[$value])) ? $this -> viewdata[$value] : '';
		}

		private function Data_2($name, $newvalue = '') {
			$this  -> viewdata[$name] = $newvalue;
			return (isset($this -> viewdata[$name])) ? $this -> viewdata[$name] : '';
		}

public function CheckModel($model) {
	$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model); 
	if(class_exists($model))  {
		return TRUE;
	}
	$this->error = 504;
	return FALSE;
}

public function CheckView($view) {
	if(file_exists(__APP__.$view.VEXT)) {
		return TRUE;
	}
	$this->error = 404;
	return FALSE;
}

public function CheckController($controller) {
	$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
	if(class_exists($controller))  {
		return TRUE;
	}
	$this->error = 503;
	return FALSE;
}
public function CheckError() {
	if(isset($this->model->error) && $this->model->error > 0)  {
		$this->error = $this->model->error;
		return TRUE;
	}
	if(isset($this->action->error) && $this->action->error > 0)  {
		$this->error = $this->action->error;
		return TRUE;
	}
	if(isset($this->error) && $this->error > 0)  {
		return TRUE;
	}
	return FALSE;
}
	public function __destruct() {
		foreach ($this as $key => $value) {
			$this -> $key = NULL;
			unset($this -> $key);
		}
		unset($this);
		clearstatcache();
	}

}
?>