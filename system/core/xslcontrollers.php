<?php
namespace System\Core ;

class XSLControllers extends \XSLTProcessor {
	const ACCESS_ANY = 1000;
	const ACCESS_USER = 500;
	const ACCESS_EDITOR = 300;
	const ACCESS_MODERATOR = 100;
	const ACCESS_SYSTEM = 1;
	const ACCESS_ADMIN = 0;
	
public $model;
public $data;
public $view;
public $action;

public $me;
public $access;
public $message;
public $error;

public function __construct($model)  
{
	try {
		if (!$this->hasExsltSupport()) {
		throw new SystemException('EXSLT not supported',510);
	}
		$this->me=get_class($this);	
		//$this->view = parent::$view;

		$this->data = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data></data>', null, false);
		if (is_object($model)) {
			$this->model = $model;
		} else {
			$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model );   
			if(!class_exists($model)) throw new SystemException('Class of Model not exist',504);
			$this->model = new \StdClass;
		}
		

		if (!isset($this -> access)):
		$this -> access = $this::ACCESS_ANY;
		endif;

		if(isset($this->exception)){
		if($this->error > 0) {
			//$this->view = VIEWS.'empty';
			throw new SystemException('Error',$this->error);
		}
		}
		if (is_null($this -> error)):
		$this -> error = 0;
		endif;

		//$this->ViewSync(); 
		} catch (SystemException $e){
			$this->error = $e->Code();
			$this->message = $e->Message();
			if(isset($this->exception)){
				return FALSE; 
			}
			return FALSE;
		}
		//$this->invoke();	
} 
	


	final public function Exceptions($model,$view,$controller) {
		$this->exception = new \System\Core\XSLViews($view);
		$this->exception->register($controller,$model);
	}

final public function CheckModel($model) {
	$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model); 
	if(class_exists($model))  {
		return TRUE;
	}
	$this->error = 504;
	return FALSE;
}

final public function CheckView($view) {
	if(file_exists(__APP__.$view.XVEXT)) {
		return TRUE;
	}
	$this->error = 404;
	return FALSE;
}

final public function CheckController($controller) {
	$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
	if(class_exists($controller))  {
		return TRUE;
	}
	$this->error = 503;
	return FALSE;
}
final public function CheckError() {
	if(isset($this->model->error) && $this->model->error > 0)  {
		$this->error = $this->model->error;
		return TRUE;
	}
	if(isset($this->action->error) && $this->action->error > 0)  {
		$this->error = $this->action->error;
		return TRUE;
	}
	if(isset($this->error) && $this->error > 0)  {
		return TRUE;
	}
	return FALSE;
	}
	public function ViewData() {
			$argsv = func_get_args();
			$argsc = func_num_args();
			if (method_exists($this, $f = 'Data_' . $argsc)) {
				return call_user_func_array(array($this, $f), $argsv);
			}
		}

	final private function Data_1($value = '') {
		return (isset($this ->data->$value)) ? $this ->data->$value : '';
	}

	final private function Data_2($name, $newvalue = '') {
		if($this ->data instanceof \SimpleXMLElement){
			$this->data->addChild($name,$newvalue);
		} else {
			$this ->data->$name = $newvalue;
		}
		return (isset($this ->data->$name)) ? $this ->data->$name: '';
	}

	public function toArray(\SimpleXMLElement $xml)  {
		return json_decode(json_encode( $xml),TRUE);
	}
	public function __destruct() {
		foreach ($this as $key => $value) {
			$this -> $key = NULL;
			unset($this -> $key);
		}
		unset($this);
		clearstatcache();
	}

}
?>