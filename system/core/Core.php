<?php
namespace System\core ;
use \System\Core\Router as router;
use \System\Core\Helper as Helper;
use \System\Core\Intl as Intl;

class Core extends \XSLTProcessor {
	const ACCESS_ANY = 1000;
	const ACCESS_USER = 500;
	const ACCESS_EDITOR = 300;
	const ACCESS_MODERATOR = 100;
	const ACCESS_SYSTEM = 1;
	const ACCESS_ADMIN = 0;
	
public $model;
public $data;
public $view;
public $action;

public $name;
public $access;
public $message;
public $error;

public static $obj;

   final public function __construct() {
		try {
		$retval = NULL;
		if (!$this->hasExsltSupport()) {
            throw new SystemException('EXSLT not supported',510);
        }
		$this->name=get_class($this);	
		$this->data = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><data/>', null, false);

		if (!isset($this -> access)):
			$this -> access = $this::ACCESS_ANY;
		endif;

		if (is_null($this -> error)):
			$this -> error = 0;
		endif;

        $argsv = func_get_args();
        $argsc = func_num_args();
        if (method_exists($this, $f = 'load_' . $argsc)) {
            $retval = call_user_func_array(array($this, $f), $argsv);
        }
		if($this->view==NULL){
		//	 throw new SystemException("App View not Definied",400);
		}
		if($this->error > 0) {
		if(isset($this->exception)){
			//$this->view = VIEWS.'empty';
			throw new SystemException($this->message,$this->error);
		}
		}
        } catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final private function load_1($view = '') {
		try {
		if (!$this->CheckView($view)) throw new SystemException("View not exists",404);
        $this->view = $view;
       // $this->model = new \StdClass;

		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final private function load_2($model,$view) {
        try {
		if (!$this->CheckView($view)) throw new SystemException("View not exists",404);
        $this->view = $view;
		if (is_object($model)) {
			$this->model = $model;
		} else {
			$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model );   
			if(!class_exists($model)) throw new SystemException('Class of Model not exist',304);
			//$this->model = new \StdClass;
		}
		if($this->model==NULL){
			 throw new SystemException("App Model not Definied",304);
		}
		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
	public function Init(){
		return TRUE;
	}
	final public function Exceptions($model,$view,$controller) {
		if (is_object($controller)) {
			$this->exception = $controller;
		} else {
			$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
			if(!class_exists($controller)) return FALSE;
			$this->exception = new $controller($model,$view); 
		}
		$this->exception->ViewData('message', $this->message);
		$this->exception->ViewData('error', $this->error);
		$this->exception->Init();
	}

final public function CheckModel($model) {
	$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model); 
	if(class_exists($model))  {
		return TRUE;
	}
	$this->error = 504;
	return FALSE;
}

final public function CheckView($view) {
	if(file_exists(__APP__.$view.XVEXT)) {
		return TRUE;
	}
	$this->error = 404;
	return FALSE;
}

final public function CheckController($controller) {
	$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
	if(class_exists($controller))  {
		return TRUE;
	}
	$this->error = 503;
	return FALSE;
}
final public function CheckError() {
	if(isset($this->model->error) && $this->model->error > 0)  {
		$this->error = $this->model->error;
		return TRUE;
	}
	if(isset($this->error) && $this->error > 0)  {
		return TRUE;
	}
	return FALSE;
	}
	final public function ViewData() {
			$argsv = func_get_args();
			$argsc = func_num_args();
			if (method_exists($this, $f = 'Data_' . $argsc)) {
				return call_user_func_array(array($this, $f), $argsv);
			}
		}

	final private function Data_1($value = '') {
		return (isset($this ->data->$value)) ? $this ->data->$value : '';
	}

	final private function Data_2($name, $newvalue = '') {
		if($this ->data instanceof \SimpleXMLElement){
			$this->data->addChild($name,$newvalue);
		} else {
			$this ->data->$name = $newvalue;
		}
		return (isset($this ->data->$name)) ? $this ->data->$name: '';
	}

	public function toArray(\SimpleXMLElement $xml)  {
		return json_decode(json_encode( $xml),TRUE);
	}
	public function __destruct() {
		self::$obj==NULL;
		foreach ($this as $key => $value) {
			$this -> $key = NULL;
			unset($this -> $key);
		}
		unset($this);
		clearstatcache();
	}
        
    final public function Show($view = NULL) {
        echo $this->view($view);
    }

    final public function View($path=NULL) {
        # code...
        try {
			self::$obj =& $this;
			$this->action = $this->Init();
			if($path!=NULL) $this->view=$path;
			if($this->view==NULL){
				 throw new SystemException("View not Definied",401);
			}
			if (!$this->CheckView($this -> view)){
				 throw new SystemException("View not exists",404);
			}		
			//if($this->error > 0) throw new SystemException($this->message,$this->error);	

            if($this->error > 0) {
            if(isset($this->exception)){
                    throw new SystemException($this->message,$this->error);
                }
            }
			$view = new \DOMDocument();
			$view->loadXML(file_get_contents(__APP__ .$this->view. XVEXT));
			$this->setParameter('', 'self', '\\'.get_called_class().'::Call');
            $this->importStylesheet($view);
			$retval = $this->transformToXML($this->data); 
			self::$obj=NULL;
			echo self::Trace();
            return $retval;
        } catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
			if(isset($this->exception)){
            	$this->exception->ViewData('error' ,$e->Code());
            	$this->exception->ViewData('message' ,$e->Message());
				return $this->exception->view();
			}
            return FALSE;
        }
    }

	final public function Register($view, $model,$controller) {
		if (is_object($controller)) {
			$this->$controller = $controller;
		} else {
			$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
			if(!class_exists($controller)) return FALSE;
			$this->$controller = new $controller($model,$view); 
		}
		$this->$controller->Init();
	}

    final public static function Call($method, $parameters=""){
		$a = "\\".get_called_class()."::".$method;
		if(self::$obj !== NULL && method_exists(self::$obj, $method))
        return call_user_func_array(array(self::$obj, $method), explode(";", $parameters));
		//return FALSE;
    }

   public static function Trace(){
	if(defined('TRACE')){
    $e = new SystemException();
    $trace = explode("\n", $e->getTraceAsString());
    // reverse array to make steps line up chronologically
    $trace = array_reverse($trace);
    array_shift($trace); // remove {main}
    array_pop($trace); // remove call to this method
    $length = count($trace);
    $result = array();
   
    for ($i = 0; $i < $length; $i++)
    {
        $result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
    }
   
    return nl2br("\t" . implode("\n\t", $result));
	}
}  
}
?>