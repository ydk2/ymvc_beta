<?php
namespace System\Core;
	use \System\Libraries\Classes\toAscii as toAscii;
	use \System\Data\Config\Config as Config;
/**
 *
 */

class DBConnect {
	public $db;
	
	public function Connect($engin, $database, $host = 'localhost', $user = NULL, $pass = NULL) {
		try {
			if ($engin == 'posql') {
				require_once __APP__.VENDORS.'posql.php';
				$database_name = __APP__ . '/system/data/database/' . $database . '.db';
				//echo $database_name;
				if (!file_exists($database_name.'.php')) {
					throw new SystemException('Database not exist.',420404);
				}
				// try connect
				//$sql = SQL;
				
				$this->db = new \Posql($database_name);
				//$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

				if ($this->db -> isError()) {
					abort($this->db);
					throw new SystemException('Can\'t connect to Database.',420502);
				}

			}
			if ($engin != 'posql') {
			try {
				if ($user === NULL || $pass === NULL) {
					throw new SystemException('User and Password not filed.');
				}
				$this->db = new \PDO($engin.':host=' . $host . ';dbname=' . $database, $user, $pass);
				$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

				$err = $this->db->errorInfo();
				if($err[0]>0){
					throw new SystemException( $Exception->getMessage( ) , (int)$Exception->getCode( ) );
				}

			} catch (\PDOException $e){
    			//handle PDO
    			throw new SystemException( $e->getMessage( ) , (int)$e->getCode( ) );
			}
			}
		} catch (SystemException $e) {

			Helper::Enable(SVIEWS . 'shared/Exception');

			$m = new \StdClass();
			$m->error = $e->code();
			$v = new Views($m,SVIEWS.'errors/errors',SCONTROLLERS.'errors/errors');
			$v->ViewData('header',"Database error!");
			$v->ViewData('content',$e->message().": 4201".$e->code());
			Helper::Disable(SVIEWS . 'shared/Exception');

			exit($v->Show());
		}

	}

	public function __destruct() {
		$this->db = NULL;
		unset($this->db);
	}

}
?>