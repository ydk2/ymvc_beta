<?php
namespace System\core;
/**
*
*/

class XSLViews extends XSLControllers {
 /*   
	const ACCESS_ANY = 1000;
	const ACCESS_USER = 500;
	const ACCESS_EDITOR = 300;
	const ACCESS_MODERATOR = 100;
	const ACCESS_SYSTEM = 1;
	const ACCESS_ADMIN = 0;
    
    public $model;
    public $data;
    public $view;
    public $action;
    
    public $me;
    public $access;
    public $message;
    public $error;
    */
   final public function __construct($view) {
		try {
		if (!$this->hasExsltSupport()) {
            throw new SystemException('EXSLT not supported',510);
        }
        $argsv = func_get_args();
        $argsc = func_num_args();
        if (method_exists($this, $f = 'start_' . $argsc)) {
            return call_user_func_array(array($this, $f), $argsv);
        }
        } catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final private function start_1($view = '') {
		try {
		if (!$this->CheckView($view)) throw new SystemException($this->action->message,404);
        $this->view = $view;
		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final private function start_2($view,$controller) {
        try {
		if (!$this->CheckView($view)) throw new SystemException($this->action->message,404);
        $this->view = $view;
        $model = new \StdClass;
		
        $this->register($controller,$model);
		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final private function start_3($model,$view,$controller) {
        try {
		if (!$this->CheckView($view)) throw new SystemException($this->action->message,404);
        $this->view = $view;
        $this->register($controller,$model);
		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
            return FALSE;
        }
    }
    
    final public function Show($view = NULL) {
        echo $this->view($view);
    }
    
    final public function ViewData() {
        $args = func_get_args();
        return call_user_func_array(array($this->action, __FUNCTION__), $args);
    }
    
    public function __destruct() {
        foreach ($this as $key => $value) {
            $this->$key = NULL;
            unset($this->$key);
        }
        unset($this);
        clearstatcache();
    }
    
    final public function View($path=NULL) {
        # code...
        try {
            //$view = new \DOMDocument();
            //$this->load("pages.xsl");
			if($path!=NULL) $this->view=$path;
			if($this->action->view==NULL){
				$this->action->view=&$this->view;
			}
			if (!$this->CheckView($this -> view)){
				 throw new SystemException("View not exists",404);
			}
			if(is_null($this->action)) throw new SystemException('Internal error',500);
			if (!$this->CheckView($this->action->view)){
				$this->action->error = 404;
				 throw new SystemException($this->action->message,$this->action->error);
			}
			
					
			//if($this->action->error > 0) throw new SystemException($this->action->message,$this->action->error);	
            if(isset($this->action->exception)){
                if($this->action->error > 0) {
                    //$this->action->view = VIEWS.'empty';
                    throw new SystemException($this->action->message,$this->action->error);
                }
            }
			
            if($this->view !== $this->action->view){
                $this->view=$this->action->view;
            }

			$view = new \DOMDocument();
			$view->loadXML(file_get_contents(__APP__ .$this->view. XVEXT));
			//$view->documentURI = __APP__ .$this->view. XVEXT;
            //$view = new \SimpleXMLElement(__APP__ .$this->view. XVEXT, NULL, TRUE);
            //$xsl = new \XSLTProcessor();
            $this->action->importStylesheet($view);
			//$data->content .= htmlentities(dump($data)); 
            return $this->action->transformToXML($this->action->data); 
        } catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
			if(isset($this->action->exception)){

            	$this->action->exception->action->ViewData('error' ,$e->Code());
            	$this->action->exception->action->ViewData('message' ,$e->Message());
				return $this->action->exception->view();
			}
            return FALSE;
        }
    }
	final public function Register($controller, $model) {
		try {

       	if (is_object($controller)) {
			//if(!($controller instanceof XSLControllers)) throw new SystemException($this->action->message,504);
			$this->action = $controller;
		} else {
			$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
			if(!class_exists($controller)) throw new SystemException($this->action->message,503);
			//$this->preSync();
			$this->action = new $controller($model); 
			//if(!($controller instanceof XSLControllers)) throw new SystemException($this->action->message,504);
			if(!$this->action) throw new SystemException($this->action->message,500);
		}
 
		$this->access=min($this->action->access,$this->access);
		if(is_null($this->action->view)){
		$this->action->view=&$this->view;
		}
		$this->me=$this->action->me;
		//if(isset($this->action->exception)){
		if($this->action->error > 0) throw new SystemException($this->action->message,$this->action->error);
		//}
		//var_dump($this);
		//if($this->action->error > 0) throw new SystemException($this->action->message,20300);
		} catch (SystemException $e){
            $this->error = $e->Code();
            $this->message = $e->Message();
			if(isset($this->action->exception)){
				//return $this->action->exception->view();
			}
			return FALSE;
		}
	}
    
}
?>