<?php
namespace System\core ;
use \System\core\Router as router;
use \System\core\Views as Views;
use \System\helpers\Intl as Intl;

class Loader  {
	const ACCESS_ANY = 9;
	const ACCESS_USER = 8;
	const ACCESS_EDITOR = 7;
	const ACCESS_MODERATOR = 2;
	const ACCESS_SYSTEM = 1;
	const ACCESS_ADMIN = 0;

	public $model;
	public $view;
	public $action;

	public $me;
	public $access;
	public $dataview;
	public $error;

public function __construct($model=null,$view=null,$controller=null)  {

       	if (file_exists(__APP__.$view.VEXT)) {
			$this->view = new Views($view);
		}

       	if (is_object($model)) {
			$this->model = $model;
		} else {
			$model = str_replace(DIRECTORY_SEPARATOR, '\\', $model );  
        	$this->view->model = new $model();
		}

       	if (is_object($controller)) {
			$this->action = $controller;
		} else {
			$controller = str_replace(DIRECTORY_SEPARATOR, '\\', $controller); 
	       	$this->view->action = new $controller($this->view->model); 
		}


		if (!isset($this -> access)):
		$this -> access = $this::ACCESS_ANY;
		endif;	
		//var_dump($this);
    } 

	public function get($attr) {
		
	}	
	public function set($attr,$val) {
		
	}

	public function __destruct() {
		foreach ($this as $key => $value) {
			$this -> $key = NULL;
			unset($this -> $key);
		}
		unset($this);
		clearstatcache();
	}

}
?>