<?php
namespace System\core;
class Caches {
	private static $cachefile;
	private static $cache;
	public static function Init($prefix = '') {
		if($prefix==''){
			$prefix = time();
		} else {
			$prefix = $prefix;
		}
		$cachefile = __APP__."/data/cache/".$prefix;
		if(!file_exists(Helper::session($prefix))){
		Helper::session_set($prefix,$cachefile);
		file_put_contents(Helper::session($prefix), json_encode(NULL,JSON_UNESCAPED_UNICODE));
		}
		return $prefix;
	}
	public static function Get($value,$prefix) {
		$data = json_decode(file_get_contents(Helper::session($prefix)),true);
		
		return $data[$value];
	}
	public static function Set($key,$value,$prefix='') {
		$data = json_decode(file_get_contents(Helper::session($prefix)),TRUE);
		$data[$key]=$value;
		file_put_contents(Helper::session($prefix), json_encode($data,JSON_UNESCAPED_UNICODE));
	}
	public static function Close($prefix) {
		unlink(Helper::session($prefix));
		Helper::session_unset($prefix);
		clearstatcache();
	}

	
}
?>