<?php 
namespace  {
global $error;
$error = 0;
function Loader( $Class ) {
    $Class = str_replace( __NAMESPACE__.'\\', '/', $Class );
    // Correct DIRECTORY_SEPARATOR
    $Class = strtolower( str_replace( array( '\\', DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR, __APP__.DIRECTORY_SEPARATOR.$Class.'.php' ));
    if( false === ( $Class = realpath( $Class ) ) ) {
        //echo "<h1>File not found in $Class</h1>";
        return false;
    } else {
        require_once( strtolower( $Class ));
        return true;
    }
}
	if (!function_exists('iconv') && function_exists('libiconv')) {
    function iconv($input_encoding, $output_encoding, $string) {
        return libiconv($input_encoding, $output_encoding, $string);
    }
	}
function seterrorcode($code)
{
	global $error;
	if($code != NULL)
	$error = $code;	
}
function geterrorcode()
{
	global $error;
	return $error;	
}

spl_autoload_register("Loader");
}
?>